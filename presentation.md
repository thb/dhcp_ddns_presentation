---
author: 'Robert Beilich, Philipp Grigarzik, Andre Thiele'
title: 'IP-Addressenverwaltung mit DHCP und DDNS'
...

# DHCP
## Problemstellung
- begrenzter IP-Adressraum (vorrangig IPv4)

# DHCP
## Lösungsansatz
- 2000 Nutzer, von denen nur 500 gleichzeitig aktiv sind, brauchen nur 500 statt 2000 Adressen
- Bspw. vergeben ISP's IP-Adressen nach Bedarf dynamisch an Kunden

\   

- Dynamic Host Configuration Protocol (RFC 2131)
- Hosts erhalten durch DHCP weitere Informationen:
    - Subnetzmaske
    - Standardgateway (First Hop Router)
    - Adresse des lokalen DNS-Servers

# DHCP
## Technische Funktionsweise
::: columns

:::: column
:::: paragraph
\ 
::::
:::: paragraph
Phase 1: DHCPDISCOVER
::::
:::: paragraph
\ 
::::
:::: paragraph
Client sendet DHCPDISCOVER
::::
:::: paragraph
\ 
::::
:::: paragraph
DST: 255.255.255.255:67
::::
:::: paragraph
SRC: 0.0.0.0:68
::::
::::

:::: column
![diagram](./graphics/dhcp_diagram.png)\ 
::::

:::

# DHCP
## Technische Funktionsweise
::: columns

:::: column
:::: paragraph
\ 
::::
:::: paragraph
Phase 2: DHCPOFFER
::::
:::: paragraph
\ 
::::
:::: paragraph
Server antwortet mit DHCPOFFER
::::
:::: paragraph
\ 
::::
:::: paragraph
DST: 255.255.255.255:68
::::
:::: paragraph
SRC: a.b.c.d:67
::::
:::: paragraph
\ 
::::
:::: paragraph
Konfigurationsparameter: Transaktions-ID, Lease-Time, vorgeschlagene Adresse des Clients, Netzmaske, DNS-Server, Gateway
::::
::::

:::: column
![diagram](./graphics/dhcp_diagram.png)\ 
::::

:::

# DHCP
## Technische Funktionsweise
::: columns

:::: column
:::: paragraph
\ 
::::
:::: paragraph
Phase 3: DHCPREQUEST
::::
:::: paragraph
\ 
::::
:::: paragraph
Client sucht aus Server Angeboten aus und antwortet auf jeweiliges Angebot mit DHCPREQUEST
::::
:::: paragraph
\ 
::::
:::: paragraph
DST: 255.255.255.255:67
::::
:::: paragraph
SRC: 0.0.0.0:68
::::
:::: paragraph
\ 
::::
:::: paragraph
Sendet die ihm vorgeschlagenen Parameter und die Adresse des DHCP-Servers zurück
::::
::::

:::: column
![diagram](./graphics/dhcp_diagram.png)\ 
::::

::::

# DHCP
## Technische Funktionsweise
::: columns

:::: column
:::: paragraph
\ 
::::
:::: paragraph
Phase 4: DHCPACK
::::
:::: paragraph
\ 
::::
:::: paragraph
Server antwortet Client mit DHCPACK und bestätigt die angeforderten Parameter
::::
:::: paragraph
\ 
::::
:::: paragraph
DST: e.f.g.h:68
::::
:::: paragraph
SRC: a.b.c.d:67
::::
::::

:::: column
![diagram](./graphics/dhcp_diagram.png)\ 
::::

:::

# DHCP
## Technische Funktionsweise
- Nach DHCPACK kann der Client die IP Adresse für die angegebene Lease-Time (Leihdauer) verwenden
- Diese kann vom Client verlängert werden

# DDNS
## Problemstellung
- durch DHCP kann die IP-Adresse dynamisch sein
- trotzdem soll ein Dienst mit einem festen Domain-Namen erreicht werden

# DDNS
## Lösungsansatz
- DNS-Server automatisiert über die aktuelle IP-Adresse informieren

# DDNS
## Technische Funktionsweise
- Server informiert DDNS-Server über die erhaltene IP-Adresse (authentifiziert)
    - Alternative im internen Netz: DHCP-Server informiert
- Standardvorgehen eines DNS-Servers
    - DDNS-Server speichert die Zuordnung
    - bei Verbindungsaufbau Abfrage der IP beim DDNS-Server

# DDNS
## Technische Funktionsweise
- Server (oder DHCP-Server) informiert DDNS-Server über Änderungen der IP-Adresse
- TTL wird besonders gering gehalten, um die Änderung schnell zu propagieren

# Quellen
- "Computernetzwerke: Der Top Down Ansatz" (S. 388)
- https://www.cloudflare.com/learning/dns/glossary/dynamic-dns/

- https://en.wikipedia.org/wiki/
  Dynamic_Host_Configuration_Protocol#/
  media/File:DHCP_session.svg
